const button1 = document.getElementById("btn-example");
const button2 = document.getElementById("btn-example2");

button1.addEventListener("click", function () {
    // console.log(this); - this - это ссылка на DOM-элемент, на котором произошло событие
    this.style.backgroundColor = "red";
    this.style.color = "white";
});

// addEventListener в отличие от .onclick позволяет повесить 2 события-обработчика
button1.addEventListener("click", function () { 
    console.log("Второе событие");
});

button2.addEventListener("click", inverseStyle); // в качестве второго аргумента можно указать не только анонимную функцию, но и имя (без круглых скобок после!) созданой ранее функции

function inverseStyle() {
    console.log(this)
    this.style.backgroundColor = "red";
    this.style.color = "white";
}


