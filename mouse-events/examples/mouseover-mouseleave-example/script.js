const button1 = document.getElementById("btn-example");
const button2 = document.getElementById("btn-example2");

// событие mouseover наступает когда пользователь наводит мышку на элемент. Обратите внимание! Когда он ее убирает, обратно ничего не возвращается, потому что мы не повесили на button1 обработчик события mouseleave (курсор уходит за границы элемента)
button1.addEventListener("mouseover", increaseButton);


button2.addEventListener("mouseover", increaseButton);

// событие mouseleave наступает когда курсор уходит за границы элемента
button2.addEventListener("mouseleave", setNormalSize);

function increaseButton() {
    this.style.transition = "0.4s";
    this.style.padding = "20px 25px";
}

function setNormalSize() {
    this.style.padding = "15px 10px";
}