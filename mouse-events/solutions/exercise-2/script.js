/*
Напишите код, который работает так: при клике на пункт меню
его фон меняется на красный. Но при этом если до этого был выбран
другой пункт меню, у предыдущего выбраного пункта красный фон меняется обратно на черный
(чтобы не получилось два красных пункта меню) 
*/
const topMenuItems = document.getElementsByClassName("navbar-nav-link");

for (let i = 0; i < topMenuItems.length; i++) {
    topMenuItems[i].addEventListener("click", function () {
        const menuLinks = this.parentElement.children;
        for (const link of menuLinks) {
            link.style.backgroundColor = "black";
        }
        /*
        for(let y = 0; y < menuLinks.length; y++) {
            const link = menuLinks[0];
        }
        */
        this.style.backgroundColor = "red";
    });

}
