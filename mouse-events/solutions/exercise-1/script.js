/*
 Сделайте так, чтобы при первом клике на кнопку ее
фон менялся на красный, цвет текста -на белый,
а текст внутри кнопки на "Eat me!".
При повторном клике все возвращалось обратно.
Код нужно писать руками, а не копировать из примера.
*/

const btn1 = document.getElementById('first-button');
btn1.addEventListener("click", function() {
    if (this.textContent === "Click me!") {
        this.style.backgroundColor = "red";
        this.style.color = "white";
        this.textContent = "Eat me!";
    } else {
        this.textContent = "Click me!";
        this.style.backgroundColor = "white";
        this.style.color = "red";
    }
});