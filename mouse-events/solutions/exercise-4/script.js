 /*
Напишите код, который работает так: пользователь вводит дело в поле ввода,
нажимает на кнопку Add case, и оно появляется в списке
 */

const buttonAddCase = document.getElementById("add-case");
const fieldCaseName = document.getElementById("case-name");
const caseList = document.getElementById("case-list");

buttonAddCase.addEventListener("click", function() {
    if (fieldCaseName.value) {
        const newCase = fieldCaseName.value;
        caseList.insertAdjacentHTML("beforeend", `<li>${newCase}</li>`);
        fieldCaseName.value = "";
    }
});