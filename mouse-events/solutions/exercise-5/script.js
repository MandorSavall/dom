 /*
Напишите код, который работает так:
1. При клике на кнопку с id="myBtn" появляется модальное окно с id="myModal".
2. При клике на крестик внутри модального окна оно закрывается.
 */

 const modalButton = document.getElementById('myBtn')
 modalButton.addEventListener('click', showModal);

 const modalClose = document.querySelector('.modal .close');
 modalClose.addEventListener('click', closeModal);

function showModal() {
   const modalLink = modalButton.dataset.target;
   const modalWindow = document.querySelector(modalLink);
   modalWindow.style.display = 'block';    
}

function closeModal() {
     const modalWindow = this.closest('.modal');
     modalWindow.style.display = 'none';    
}