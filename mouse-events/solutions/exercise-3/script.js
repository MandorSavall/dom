/*
Напишите код, который работает так:
1. При наведении на элемент списка его фон меняется на светло-красный 
и у него появляется нижнее подчеркивание (border-bottom).
2. При клике на элемент списка его шрифт становится жирным, а фон - светло-серым. 
*/

const list = document.querySelectorAll('.people-list li');

list.forEach(elem => elem.addEventListener('mouseover', addElemSelection));

list.forEach(elem => elem.addEventListener('mouseout', removeElemSelection));

list.forEach(elem => elem.addEventListener('click', makeElemBold));

function addElemSelection() {
    this.style.backgroundColor = 'grey';
    this.style.borderBottom = '1px solid black';
}

function removeElemSelection() {
    this.style.backgroundColor = '';
    this.style.borderBottom = '';
}

function makeElemBold() {
    this.style.fontWeight = 'bold';
    this.style.backgroundColor = 'lightgrey';
}
