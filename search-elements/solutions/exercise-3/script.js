/*
1. Уберите у списка маркеры.
2. Сделайте шрифт в элементах списка жирным.
3. Сделайте каждый второй элемент списка - на сером фоне.
Используйте children
*/
const list = document.getElementById("people-list");
list.style.listStyle = "none";
list.style.fontWeight = "bold";
const listItems = list.children;
for (let i = 1; i < listItems.length; i += 2) {
    listItems[i].style["background-color"] = "grey";
}
