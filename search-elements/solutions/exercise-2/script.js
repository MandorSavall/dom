/*Задание: 
    1. Уберите у списка маркеры.
    2. Сделайте шрифт в элементах списка жирным.
    3. Сделайте каждый второй элемент списка - на сером фоне.
   Используйте getElementById и getElementsByClassName
*/

const peopleList = document.querySelector("#people-list")
peopleList.style.listStyle = "none";

const listItems = document.querySelectorAll("#people-list .person")
listItems.forEach(function (elem, index) {
    elem.style.fontWeight = "bold";
    (index % 2) ? elem.style.backgroundColor = "grey": "";
});
