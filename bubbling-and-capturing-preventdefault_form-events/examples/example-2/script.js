const buttonExample = document.getElementById("btn-example");

buttonExample.addEventListener("click", function (e) { // объект события часто сокращают до e
    e.preventDefault();
    /*
    e.preventDefault() - метод, отменяющий стандартную реакцию браузера на событие ДЛЯ ЭТОГО ТЕГА. 
    В случае со ссылкой это - переход на другую страницу. Даже если в атрибуте href не указан адрес, а просто стоит заглушка "#", то браузер все равно попытается открыть новую страницу, в результате чего ваша текущая страница прокрутится вверх
    */
    e.target.classList.toggle("active");
});
