/*
Напишите код, который работает так:
1. Пользователь вводит дело в поле ввода, нажимает на кнопку Add case, и оно появляется в списке.
2. При наведении на дело в правом верхнем углу появвляется крестик, при нажатии на который дело удаляется из списка.
*/

const addCaseForm = document.getElementById("add-case");
const caseList = document.getElementById("case-list")

addCaseForm.addEventListener("submit", addCase);

caseList.addEventListener("click", removeCase);

function addCase(e) {
    e.preventDefault();
    const caseText = this.querySelector("[name='case-name']").value;
    caseList.insertAdjacentHTML("beforeend", `<li class="case-item">${caseText}<span class="close">&times;</span></li>`);
}

function removeCase(e) {
    if (e.target.classList.contains("close")) {
        const closeCaseItem = e.target.closest("li");
        closeCaseItem.remove();
    }
}