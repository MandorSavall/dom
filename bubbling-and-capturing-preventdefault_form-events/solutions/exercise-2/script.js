/*
Напишите код, который работает так:
1. при клике на кнопку с id="myModal" на странице появляется всплывающее окно, селектор которого хранится в атрибуте "data-target" кнопки.
2. При клике на крестик внутри или на серую область за пределами окна - окно закрывается.
*/

const modalButtons = document.querySelectorAll('.btn-modal');
modalButtons.forEach(button => button.addEventListener('click', openModal));

const modalsOverlay = document.querySelectorAll('.modal');
modalsOverlay.forEach(overlay => overlay.addEventListener('click', closeModal));

function openModal(e) {
    e.preventDefault();
    const modalWindowSelector = this.dataset.target;
    const modalWindow = document.querySelector(modalWindowSelector);
    modalWindow.classList.add('active');
}

function closeModal(e) {
    if (e.target.matches(".modal") || e.target.matches(".close")) {
        this.classList.remove('active');
    }
}