 /*
Напишите код, который работает так: 
1. При  клике на пункт меню к нему добавляется класс active.
2. У остальных пунктов меню этот класс убирается.
Используйте всплытие событий и e.target
*/

 const topMenu = document.getElementById('top-menu');
 topMenu.addEventListener('click', function (event) {
     const prevActiveItem = this.querySelector('.active');
     if (prevActiveItem) {
         prevActiveItem.classList.remove('active');
     };
     event.target.classList.add('active');
 });