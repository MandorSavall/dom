/*
Напишите код, который работает так:
1. Пользователь выбирает одного врача из списка. В зависимости от выбраного врача, 
под выпадающим списком, внутри div c id="additonal-fields" появляютя дополнительные поля ввода:
- Кардиолог: возраст; индекс массы тела.
- Терапевт: возраст; диагнозы.
- Стоматолог: дата последнего визита.
2. Когда пользователь нажимает "Записаться": 
- если выбран врач, заполнены все обязательные поля (возраст для кардиолога и терапевта, дата последнего визита для стоматолога), 
выбрана дата визита и установлена галочка для "Подтверждаю правильность введной информации" - под формой выводится надпись 
"Поздравляю, вы успешно записались на датаВизита";
- если же что-то не заполнено - под незаполненым полем появляется надпись "Это поле обязательно для заполнения"
*/

const doctorsList = document.getElementById('select-doctor');
doctorsList.addEventListener('change', addAdditionalFields);

const doctorForm = document.getElementById('appointment-to-doctor');
doctorForm.addEventListener('submit', function (e) {
    e.preventDefault();
    console.log(this.querySelector("[name='agree']").checked)
});

function addAdditionalFields() {
    const additionalFields = {
        cardio: `<input id="age" type="number" placeholder="enter your age">, 
                    <input id="bodyWeightIndex" type="number" placeholder="enter your body weight index"> `,
        therapist: `<input id="age" type="number" placeholder="enter your age">, 
                    <input id="diagnose" type="text" placeholder="enter your diagnose">`,
        dentist: `<label for="previous-visit-date">Дата последнего визита</label>
                    <input id="previous-visit-date" type="date" placeholder="enter your previous date">`
    }

    const form = this.closest("form");
    const additionalFieldsElement = form.querySelector("#additonal-fields");
    const doctorType = this.value;
    additionalFieldsElement.innerHTML = additionalFields[doctorType];
}