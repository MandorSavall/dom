/*
Напишите скрипт, который при наведении на элемент с классом tooltip создает внутр него всплывающую подсказку - 
span с классом tooltip-text. А когда курсор уходит с элемента - подсказка пропадает.
Пример его работы можно посмотреть здесь https: //www.w3schools.com/howto/tryit.asp?filename=tryhow_css_tooltip
*/
