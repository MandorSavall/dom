/* Задание:
Напишите  код, который работает так: при клике на элемент с классом btn-collapse после него должен появлятся p с текстом, храняющимся в атрибуте data-info. При повторном клике элемент с текстом должен удаляться.
*/

const collapseButtons = document.querySelectorAll(".btn-collapse");

collapseButtons.forEach(elem => elem.addEventListener("click", toggleDataInfo));

function toggleDataInfo() {
    const text = this.getAttribute("data-info");
    const nextElem = this.nextElementSibling;
    if(nextElem.textContent === text) {
        nextElem.remove();
    }
    else {
        this.insertAdjacentHTML("afterend", `<p>${text}</p>`);
//        const paragraph = document.createElement("p");
//        paragraph.textContent = text;
//        this.after(paragraph);        
    }
}