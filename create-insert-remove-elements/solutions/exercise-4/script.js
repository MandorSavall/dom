/*
Напишите скрипт, который при наведении на элемент с классом tooltip создает внутр него всплывающую подсказку - 
span с классом tooltip-text. А когда курсор уходит с элемента - подсказка пропадает.
Пример его работы можно посмотреть здесь https: //www.w3schools.com/howto/tryit.asp?filename=tryhow_css_tooltip
*/
const tooltips = document.querySelectorAll('.tooltip');
tooltips.forEach(elem => {
    elem.addEventListener('mouseover', showTooltip);
    elem.addEventListener('mouseout', hideTooltip);
});

function showTooltip(){
    // const span = document.createElement('span');
    // span.className = 'tooltip-text';
    // span.textContent = this.dataset.tooltip;
    // this.append(span);
    const tooltipText = this.dataset.tooltip;
    this.insertAdjacentHTML("beforeend", `<span class = 'tooltip-text'>${tooltipText}</span>`)    
}

function hideTooltip(){
    const span = this.querySelector('.tooltip-text');
    span.remove();    
}