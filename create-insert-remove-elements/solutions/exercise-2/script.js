 /*
Задание: спросите у пользователя через prompt элемент списка с каким текстом (Толстый котик/Корги/Ника/Маленький Йода) тут не нужен. После чего удалите его.
*/
const removeItemText = prompt("Какой  элемент удалить?");
const listItems = document.querySelectorAll("#list li");
for (const elem of listItems) {
 if (elem.textContent === removeItemText) {
     elem.remove();
     break;
 }
}
