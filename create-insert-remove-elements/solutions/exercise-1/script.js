/*
Задача: в цикле спрашивать у пользователя с помощью prompt дело, и выводить его внутрь тега с id="case-list" как элемент списка на экран. Если пользователь ввел пробел или пустую строку - дело не добавлять, но спросить следующее. 
Если он нажет Cancel - цикл прекратить.
*/
const caseList = document.getElementById('case-list');
let caseText = prompt('Дело?');
while (caseText !== null) {
    if (caseText) {
        // const listItem = document.createElement('li');
        // listItem.textContent = caseText;
        // caseList.append(listItem);
        caseList.insertAdjacentHTML('beforeend', `<li>${caseText}</li>`);
    }
    caseText = prompt('Дело?');
}